module.exports.validateNumber = (number, precision) => {
  let n = Math.abs(parseFloat(number));

  if (precision !== undefined) {
    if (precision === 0) {
      n = Math.trunc(n);
    } else {
      const p = Math.pow(10, precision);
      n = Math.trunc(n * p) / p;
    }
  }

  return n || 0;
};
