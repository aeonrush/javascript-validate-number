const tap = require('tap');

const { validateNumber } = require('../src');

tap.equal(validateNumber(), 0);
tap.equal(validateNumber(null), 0);
tap.equal(validateNumber(NaN), 0);
tap.equal(validateNumber(10), 10);
tap.equal(validateNumber(-1), 1);
tap.equal(validateNumber(10.12), 10.12);
tap.equal(validateNumber(10.12, 1), 10.1);
tap.equal(validateNumber(10.12, 3), 10.12);
tap.equal(validateNumber(10.9, 0), 10);
tap.equal(validateNumber(10.1, 0), 10);

tap.equal(validateNumber("10"), 10);
tap.equal(validateNumber("asd"), 0);
tap.equal(validateNumber(""), 0);
tap.equal(validateNumber("-1"), 1);
tap.equal(validateNumber("-1.1.1", 0), 1);
tap.equal(validateNumber("-1.12"), 1.12);
tap.equal(validateNumber("-1.12", 1), 1.1);
tap.equal(validateNumber("54.3asd"), 54.3);
tap.equal(validateNumber("54.3()Jkjk)()())", 0), 54);
